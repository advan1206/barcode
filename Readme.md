# BarCoce

* Este pequeño programa ocupa la librería [jbarcodebean-1.2.0](http://jbarcodebean.sourceforge.net/intro.html) para la generación de Códigos de Barras. Simplemente hay que poner que palabra o números queremos convertir y el nombre del archivo de salida. Al ejecutar el programa no se genera ninguna ventana, solo se obtiene una imagen en formato png.

## Instalación

* Descargar y descomprimir los archivos
* Colocar los archivos en la carpeta NetBeansProjects
* Abrir NetBeans y agregar el Proyecto
* Ejecutar

## Creador

Ing. Rubén García D. (AdvanReloaded)